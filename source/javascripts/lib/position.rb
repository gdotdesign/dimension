# Positioning module
module Position
  def set(prop, value)
    if prop == 'right'
      @style.right = (`window.innerWidth` - value).px
    elsif prop == 'bottom'
      @style.bottom = (`window.innerHeight` - value).px
    else
      @style[prop] = value.px
    end
  end
end

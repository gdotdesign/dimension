module Core
  # Simple class for point with x and y coordinates.
  class Point
    attr_accessor :left, :top

    # Creates a new instance
    #
    # @param left [Float] The left coordiante
    # @param top [Float] The top coordiante
    def initialize(left = 0, top = 0)
      @left, @top = left, top
    end

    # Returns the difference from an other point.
    #
    # @param point [Core::Point] The point to caluclate the difference from
    #
    # @return [Core::Point] The difference
    def diff(point)
      self.class.new left - point.left, top - point.top
    end
  end
end

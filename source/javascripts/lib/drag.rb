module Core
  # Class for dragging with mouse events.
  class Drag
    include Fron::Eventable
    extend  Fron::Eventable

    attr_reader :base, :body, :position

    # Creates a new drag instance.
    #
    # @param base [DOM::Element] The element to monitor
    def initialize(base)
      reset
      @base = base
      @body = DOM::Document.body

      @base.on 'mousedown' do |event| start(event) end
    end

    # Returns the current difference position
    # from the start position.
    #
    # @return [Core::Position] The difference
    def diff
      @start_position.diff @position
    end

    private

    # Runs when dragging starts.
    #
    # @param event [Event] The event
    def start(event)
      off if @pos_method

      @position      = getPosition(event)
      @start_position = @position
      @mouse_is_down   = true

      trigger 'start', event

      @pos_method = @body.on! 'mousemove' do |evt| pos(evt) end
      @up_method  = @body.on! 'mouseup'   do |evt| up(evt)  end

      requestAnimationFrame do move end
    end

    # Runs when the mouse moves starts.
    #
    # @param event [Event] The event
    def pos(event)
      event.stop
      @position = getPosition(event)
    end

    # Runs when mouse releases.
    #
    # @param event [Event] The event
    def up(event)
      reset
      event.stop
      off
      trigger 'end'
    end

    # Runs on animation frame when the mouse is down.
    def move
      requestAnimationFrame { move } if @mouse_is_down
      return unless @position
      trigger 'move'
    end

    # Removes event listeners
    def off
      @body.off 'mousemove', @pos_method
      @body.off 'mouseup',   @up_method
    end

    # Resets the drag
    def reset
      @position      = nil
      @start_position = nil
      @mouse_is_down   = false
    end

    # Gets the position from the given event.
    # @param event [Event] The event
    #
    # @return [Core::Point] A point from the event.
    def getPosition(event)
      Point.new event.pageX, event.pageY
    end
  end
end

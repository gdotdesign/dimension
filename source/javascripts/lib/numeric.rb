# Numeric
class Numeric
  # Returns the px representation
  #
  # @return [String] The px
  def px
    "#{self}px"
  end
end

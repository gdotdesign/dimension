module Dimensions
  # Position Dimension
  class Position < Dimension
    CLASS_MAP = {
      left:   'horizontal',
      right:  'horizontal',
      top:    'vertical',
      bottom: 'vertical'
    }

    component :switch, 'i.fa.fa-refresh'

    on :click, '.fa-refresh', :switch

    def resolve
      @base.resolve @side, @target.send(@to).send(@direction, @input.text.to_i)
    end

    def switch
      @direction = @direction == '+' ? '-' : '+'
      trigger 'update'
    end

    def initialize
      super
      @direction  = @options[:direction] || '+'
      @target     = @options[:target]
      @to         = @options[:to]
      @side       = @options[:side]
      @input.text = @options[:initial]

      addClass CLASS_MAP[@side]

      if hasClass('vertical')
        @_side_first  = 'top'
        @_side_second = 'bottom'
        @_position    = 'left'
        @_size        = 'width'
      else
        @_side_first   = 'left'
        @_side_second  = 'right'
        @_position     = 'top'
        @_size         = 'height'
      end
    end

    def render
      side_values = [@target.send(@to), @base.send(@side)]
      set @_side_first, side_values.min
      set @_side_second, side_values.max
      set @_position, @base.send(@_position) + @base.send(@_size) / 2
    end
  end
end

module Dimensions
  # Size Dimension
  class Size < Dimension
    MAP = {
      width: %w(top left),
      height: %w(left top)
    }

    TYPE = {
      width: 'horizontal',
      height: 'vertical'
    }

    # component :switch, 'i.fa.fa-arrows-h'

    def initialize
      super
      @input.text = @options[:initial]
      @side = @options[:side]

      addClass TYPE[@side]
    end

    def resolve
      @base.resolve @side, @input.text.to_i
    end

    def render
      side_first  = MAP[@side][0]
      side_second = MAP[@side][1]

      set side_first, @base.send(side_first) - 20
      set side_second, @base.send(side_second)
      set @side, @base.send(@side)
    end
  end
end

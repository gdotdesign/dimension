require 'fron'

require 'lib/point'
require 'lib/numeric'
require 'lib/drag'
require 'lib/position'

require 'core/box'
require 'core/dimension'

require 'dimensions/size'
require 'dimensions/position'

# Main
class Main < Fron::Component
  component :button, 'button Add Box'

  on :click, 'button', :addBox
  on :click, 'box',    :addDimension
  on :dblclick, 'box', :addSizeDimension

  on 'remove:dimension', :removeDimension

  def initialize
    super
    @boxes = []
    @dimensions = []

    DOM::Document.body.on! 'update' do
      render
    end

    DOM::Window.on 'resize' do
      render
    end
  end

  def removeDimension(event)
    puts event.target
    @dimensions.delete event.target
    event.target.remove!
    render
  end

  def add(dimension)
    @dimensions << dimension
    self << dimension
  end

  def render
    # Find out why it is needed to
    # run it twice
    @boxes.each(&:clear)
    @dimensions.each(&:resolve)
    @dimensions.each(&:resolve)
    @boxes.each(&:clearResolved)
    @dimensions.each(&:render)
  end

  MAP = {
    left: 'height',
    top: 'width',
    right: 'height',
    bottom: 'width'
  }

  def addSizeDimension(event)
    side = MAP[event.target.getSide(event)]
    return unless side
    add Dimensions::Size.new(event.target, side: side, initial: event.target.send(side))
    render
  end

  def addDimension(event)
    target = event.target
    if @target
      selectSecond target, target.getSide(event)
    elsif event.ctrl?
      selectFirst target, target.getSide(event)
    end
  end

  def selectSecond(target, side)
    return if target == @target || !side
    return if %w(top bottom).include?(@side) && %w(left right).include?(side)
    return if %w(left right).include?(@side) && %w(top bottom).include?(side)
    puts 'Selecting second element'
    add Dimensions::Position.new(@target,
                                 target: target,
                                 side: @side,
                                 to: side,
                                 initial: 40)
    @target = nil
    render
  end

  def selectFirst(target, side)
    return unless side
    puts 'Selecting first element'
    @target = target
    @side   = side
  end

  def addBox
    box = Box.new
    add Dimensions::Size.new(box, side: :width, initial: 200)
    add Dimensions::Size.new(box, side: :height, initial: 200)
    self << box
    @boxes << box
    render
  end
end

main = Main.new
DOM::Document.body << main
main.addBox

target = nil
start_position = nil

drag = Core::Drag.new DOM::Document.body
drag.on 'start' do |event|
  return unless event.target.tag == 'box'
  target = event.target
  start_position = Core::Point.new(event.target.left, event.target.top)
end
drag.on 'move' do
  return unless target
  target.set :left, start_position.diff(drag.diff).left
  target.set :top, start_position.diff(drag.diff).top
  main.render
end
drag.on 'end' do
  target = nil
end

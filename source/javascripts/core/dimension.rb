# Dimension
class Dimension < Fron::Component
  include Position

  component :input, 'div[contenteditable=true]'

  component :remove_icon, 'i.fa.fa-remove'

  on :input, :onInput
  on :click, '.fa-remove', :onRemove

  def initialize(base, options = {})
    super 'dimension'
    @base = base
    @options = options
  end

  def onRemove
    trigger 'remove:dimension'
  end

  def onInput
    trigger 'update'
  end
end

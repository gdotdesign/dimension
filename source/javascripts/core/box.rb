# My Component
class Box < Fron::Component
  include Position

  RESOLVE_MAP = {
    left:   [:right,  :width],
    right:  [:left,   :width],
    top:    [:bottom, :height],
    bottom: [:top,    :height],
    width:  [:left,   :right],
    height: [:top,    :bottom]
  }

  SIDES = {
    top:    { x: (0..100), y: (0..20)   },
    bottom: { x: (0..100), y: (80..100) },
    right:  { x: (80..100), y: (0..100) },
    left:   { x: (0..20), y: (0..100)   }
  }

  def initialize
    super
    @style.backgroundColor = format('#%06x', (rand * 0xffffff))
    @resolves = {}
  end

  def resolve(side, value)
    return false if resolved?(side)
    set side, value
    @resolves[side] = true
  end

  def resolved?(side)
    !@resolves[side].nil?
  end

  def clear
    @resolves.clear
  end

  def clearResolved
    RESOLVE_MAP.each do |side, requirements|
      next unless requirements.select { |req| @resolves[req] }.count == 2
      @style[side] = ''
    end
  end

  def getSide(event)
    x_percent = xPercent event
    y_percent = yPercent event

    SIDES.select do |_, areas|
      areas[:x].cover?(x_percent) && areas[:y].cover?(y_percent)
    end.keys.first
  end

  private

  def xPercent(event)
    (event.pageX - left) / width * 100
  end

  def yPercent(event)
    (event.pageY - top) / height * 100
  end
end
